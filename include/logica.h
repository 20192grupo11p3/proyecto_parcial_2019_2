#ifndef LOGICA_H
#define LOGICA_H

#include "vida.h"
#include "util.h"

/* Aqui van todas las funciones que conforman la logica del tablero */

int contador;

typedef struct estadisticas{
  int num_generacion;
  int num_celvivas;
  int num_celmuertas;
  int num_celvgencero;
  int num_celmgencero;
  int prom_celvivas;
  int prom_celmuertas;
} estadisticas;

void generar_stats(char**matriz, int generacion);
void imprimir_stats(estadisticas *stats);

void revisar_arriba(char **matriz, int i, int j, int contador);
void revisar_abajo(char **matriz, int i, int j, int contador);
void revisar_izq(char **matriz, int i, int j, int contador);
void revisar_der(char **matriz, int i, int j, int contador);
void revisar_esqs_izq(char **matriz, int i, int j, int contador);
void revisar_esqs_der(char **matriz, int i, int j, int contador);
void revisar_esqi_izq(char **matriz, int i, int j, int contador);
void revisar_esqi_der(char **matriz, int i, int j, int contador);


void siguiente_generacion(char **matriz, char **newgen, int filas, int columnas, int gen, estadisticas *estads);

void sustituir_matriz(char **actual, char **next, int filas, int columnas);

void crearJuego(juego_de_vida *game, int filas, int columnas, int timesl, int generaciones);

void programa(juego_de_vida *game, juego_de_vida *next, estadisticas *estads);

void liberarMemoria(juego_de_vida *game);

#endif
