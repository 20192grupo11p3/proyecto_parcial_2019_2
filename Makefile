bin/vida: obj/main.o obj/util.o obj/logica.o
	gcc -Wall -g obj/main.o obj/util.o obj/logica.o -o bin/vida

obj/main.o: src/main.c
	gcc -Wall -c -g -I include/ src/main.c -o obj/main.o

obj/util.o: src/util.c
	gcc -Wall -c -g src/util.c -o obj/util.o

obj/logica.o: src/logica.c
	gcc -Wall -c -g -I include/ src/logica.c -o obj/logica.o

.PHONY: clean, dbg

clean:
	rm obj/*.o
	rm bin/vida

dbg:
	dbg bin/vida
