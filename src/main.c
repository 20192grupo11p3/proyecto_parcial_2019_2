#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

#include "logica.h"
#include "util.h"
#include "vida.h"

int main(int argc, char** argv){
	int filas,columnas,generaciones,valor,tiempo,celulas;
	filas = 7;
	columnas = 7;
	generaciones = -1;
	tiempo = 500000;
	celulas = 20;
	char opcion;
  while((opcion=getopt(argc,argv,"f:c:g:s:i:"))!= -1){
    switch(opcion){
      case 'f':
								filas = atoi(optarg);
								break;
			case 'c':
								columnas = atoi(optarg);
								break;
			case 'g':
								valor = atoi(optarg);
								if (valor > 0){
									generaciones = atoi(optarg);
								}
								break;
			case 's':
								tiempo = atoi(optarg);
								break;
			case 'i':
								celulas = atoi(optarg);
								break;
    }
  }

	//primero creamos los 2 juegos para manejar las generaciones
	juego_de_vida game;
	estadisticas stats;
	stats.num_generacion = 1;
  stats.num_celvivas = celulas;
  stats.num_celmuertas = 0;
  stats.num_celvgencero = celulas;
  stats.num_celmgencero = 0;
  stats.prom_celvivas = celulas;
  stats.prom_celmuertas = 0;
	crearJuego(&game,filas,columnas,tiempo,generaciones);

	juego_de_vida next;
	crearJuego(&next,filas,columnas,tiempo,generaciones);

	//luego se llena la matriz de 1s al azar llamando a la funcion de util
	llenar_matriz_azar(game.tablero,game.filas,game.columnas,celulas);

	//aqui se llama a toda la logica del programa
	programa(&game, &next, &stats);

	//aqui liberamos la memoria de los tableros creados por malloc
	liberarMemoria(&game);
	liberarMemoria(&next);

	return 0;
}
