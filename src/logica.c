#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "vida.h"
#include "util.h"

juego_de_vida game;
typedef struct estadisticas{
  int num_generacion;
  int num_celvivas;
  int num_celmuertas;
  int num_celvgencero;
  int num_celmgencero;
  int prom_celvivas;
  int prom_celmuertas;
} estadisticas;
estadisticas estadisticasgen;
int contador = 0;
void revisar_arriba(char **matriz, int i, int j, int *contador){
  if(matriz[i-1][j] == 1){*contador = *contador +1;}
}
void revisar_abajo(char **matriz, int i, int j, int *contador){
  if(matriz[i+1][j] == 1){*contador = *contador +1;}
}
void revisar_izq(char **matriz, int i, int j, int *contador){
  if(matriz[i][j-1] == 1){*contador = *contador +1;}
}
void revisar_der(char **matriz, int i, int j, int *contador){
  if(matriz[i][j+1] == 1){*contador = *contador +1;}
}
//esquinas superiores
void revisar_esqs_izq(char **matriz, int i, int j, int *contador){
  if(matriz[i-1][j-1] == 1){*contador = *contador +1;}
}
void revisar_esqs_der(char **matriz, int i, int j, int *contador){
  if(matriz[i-1][j+1] == 1){*contador = *contador +1;}
}
//esquinas inferiores
void revisar_esqi_izq(char **matriz, int i, int j, int *contador){
  if(matriz[i+1][j-1] == 1){*contador = *contador +1;}
}
void revisar_esqi_der(char **matriz, int i, int j, int *contador){
  if(matriz[i+1][j+1] == 1){*contador = *contador +1;}
}

void siguiente_generacion(char **matriz, char **newgen, int filas, int columnas, int gen, estadisticas *estads){
  int celulas_mantenidas=0;
  int celulas_muertas=0;
  int celulas_nuevas=0;

  for(int i = 0; i < filas; i++){
    for(int j = 0; j < columnas; j++){
      int contador = 0;
      //Revisar si esta en el borde superior
      if(i == 0){
        revisar_abajo(matriz,i,j,&contador);
        //Revisa si esta en la esquina izq
        if(j == 0){
          revisar_der(matriz,i,j,&contador);
          revisar_esqi_der(matriz,i,j,&contador);
        }
        //Revisa si esta en la esquina der
        else if(j == columnas-1){
          revisar_izq(matriz,i,j,&contador);
          revisar_esqi_izq(matriz,i,j,&contador);
        }
        //Resto (centro)
        else{
          revisar_der(matriz,i,j,&contador);
          revisar_esqi_der(matriz,i,j,&contador);
          revisar_izq(matriz,i,j,&contador);
          revisar_esqi_izq(matriz,i,j,&contador);

        }
        //printf("%i ",contador);
      }
      //Revisar si esta en el borde inferior
      else if(i == filas-1){
        revisar_arriba(matriz,i,j,&contador);
        //Revisa si esta en la esquina izq
        if(j == 0){
          revisar_der(matriz,i,j,&contador);
          revisar_esqs_der(matriz,i,j,&contador);
        }
        //Revisa si esta en la esquina der
        else if(j == columnas-1){
          revisar_izq(matriz,i,j,&contador);
          revisar_esqs_izq(matriz,i,j,&contador);
        }
        //Resto (centro)
        else{
          revisar_der(matriz,i,j,&contador);
          revisar_esqs_der(matriz,i,j,&contador);
          revisar_izq(matriz,i,j,&contador);
          revisar_esqs_izq(matriz,i,j,&contador);
        }
        //printf("%i ",contador);
      }
      //Revisa el centro y los lados
      else{
        revisar_arriba(matriz,i,j,&contador);
        revisar_abajo(matriz,i,j,&contador);
        //Revisa si esta en el lado izq
        if(j == 0){
          revisar_esqs_der(matriz,i,j,&contador);
          revisar_der(matriz,i,j,&contador);
          revisar_esqi_der(matriz,i,j,&contador);
        }
        //Revisa si esta en el lado der
        else if(j == columnas-1){
          revisar_esqs_izq(matriz,i,j,&contador);
          revisar_izq(matriz,i,j,&contador);
          revisar_esqi_izq(matriz,i,j,&contador);

        }
        //Resto (centro)
        else{
          revisar_esqs_der(matriz,i,j,&contador);
          revisar_der(matriz,i,j,&contador);
          revisar_esqi_der(matriz,i,j,&contador);
          revisar_esqs_izq(matriz,i,j,&contador);
          revisar_izq(matriz,i,j,&contador);
          revisar_esqi_izq(matriz,i,j,&contador);
        }
        //printf("%i ",contador);
      }
      //printf("%i ",contador);
      if(matriz[i][j]==1 && contador <= 1){
        newgen[i][j] = 0;
        celulas_muertas++;
      }
      else if(matriz[i][j]==1 && contador >= 4){
        newgen[i][j] = 0;
        celulas_muertas++;
      }
      else if(matriz[i][j]==1 && (contador==2 || contador==3)){
        newgen[i][j] = 1;
        celulas_mantenidas++;
      }
      else if(matriz[i][j]==0 && contador==3){
        newgen[i][j] = 1;
        celulas_nuevas++;
      }
      else{
	      newgen[i][j] = 0;
      }

    }
   // printf("\n");
  }
  estads->num_generacion = gen;
  estads->num_celvivas = celulas_nuevas+celulas_mantenidas;
  estads->num_celmuertas = celulas_muertas;
  estads->num_celvgencero = estads->num_celvgencero + celulas_nuevas;
  estads->num_celmgencero = estads->num_celmgencero + celulas_muertas;
  estads->prom_celvivas = estads->num_celvgencero / gen;
  estads->prom_celmuertas = estads->num_celmgencero / gen;
}

void imprimir_stats(estadisticas *estads){
  printf("Generación: %d\n",estads->num_generacion);
  printf("Numero de celulas vivas: %d\n",estads->num_celvivas);
  printf("Numero de celulas muertas: %d\n",estads->num_celmuertas);
  printf("Numero de celulas vivas desde la generacion 1: %d\n",estads->num_celvgencero);
  printf("Numero de celulas muertas desde la generacion 1: %d\n",estads->num_celmgencero);
  printf("Promedio de celulas vivas: %d\n",estads->prom_celvivas);
  printf("Promedio de celulas muertas: %d\n",estads->prom_celmuertas);
}

void sustituir_matriz(char **actual, char **next, int filas, int columnas){
	for(int i=0;i<filas;i++){
		for(int j=0;j<columnas;j++){
			actual[i][j]=next[i][j]; //aqui pasamos lo del next a actual para imprimiro y hacerlo ciclico
			next[i][j]=0; //aqui reiniciamos el tablero next por asi decirlo
		}
	}
}

void crearJuego(juego_de_vida *game, int filas, int columnas, int timesl, int generaciones){
	game->filas=filas;
        game->columnas=columnas;
        game->tiempo_sleep=timesl;
        game->generaciones=generaciones;
        game->tablero=(char **)malloc(game->filas*sizeof(char *));
        for(int i=0;i < game->filas;i++){
                *(game->tablero+i)=(char *)malloc(game->columnas*sizeof(char));
        }

}

void programa(juego_de_vida *game, juego_de_vida *next, estadisticas *estads){
        //printf("\nGeneracion 1\n");
        dibujar_grilla(game->tablero,game->filas,game->columnas);
        imprimir_stats(estads);
        //Si se le manda un numero negativo a generaciones se asume que el juego continua indefinidamente
        if(game->generaciones <0){
                int cont = 2;
                while(1){

                        siguiente_generacion(game->tablero,next->tablero,next->filas,next->columnas,cont,estads);
                        sustituir_matriz(game->tablero,next->tablero,next->filas,next->columnas);
                        usleep(game->tiempo_sleep);
                        dibujar_grilla(game->tablero,game->filas,game->columnas);
                        imprimir_stats(estads);
                        cont++;
                }
        }//caso contrario si mandamos un numero de generaciones fijo vamos a iterar el tablero tantas veces como generaciones se hayan pedido
        else {
                for(int i=2;i <= game->generaciones;i++){
                        siguiente_generacion(game->tablero,next->tablero,next->filas,next->columnas,i,estads);
                        sustituir_matriz(game->tablero,next->tablero,next->filas,next->columnas);
                        usleep(game->tiempo_sleep);
                        dibujar_grilla(game->tablero,game->filas,game->columnas);
                        imprimir_stats(estads);
                }
        }
}

void liberarMemoria(juego_de_vida *game){
        for(int i=0;i < game->filas;i++){
                free(*(game->tablero+i));
        }
        free(game->tablero);
}
